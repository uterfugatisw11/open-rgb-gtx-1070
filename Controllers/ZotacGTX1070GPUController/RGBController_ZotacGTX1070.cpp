/*-----------------------------------------*\
|  RGBController_ZotacGTX1070.cpp           |
|                                           |
|  Generic RGB Interface For OpenRGB        |
|  ZOTAC GTX 1070                           |
|                                           |
|  Martin Solohaga (Martinchoox) 12/18/2023 |
\*-----------------------------------------*/

#include "RGBController_ZotacGTX1070.h"

/**------------------------------------------------------------------*\
    @name Zotac GTX 1070
    @category GPU
    @type I2C
    @save :white_check_mark:
    @direct :white_check_mark:
    @effects :white_check_mark:
    @detectors DetectZotacGTX1070Controllers
    @comment
\*-------------------------------------------------------------------*/

RGBController_ZotacGTX1070::RGBController_ZotacGTX1070(ZotacGTX1070Controller* controller_ptr)
{
    controller = controller_ptr;

    name = "Zotac GTX 1070";
    vendor = "ZOTAC";
    description = "Zotac GTX 1070 Controller";
    location = controller->GetDeviceLocation();

    type = DEVICE_TYPE_GPU;

    mode Direct;
    Direct.name = "Direct";
    Direct.value = ZOTAC_GPU_MODE_STATIC;
    Direct.flags = MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_BRIGHTNESS;
    Direct.color_mode = MODE_COLORS_MODE_SPECIFIC;
    Direct.colors_min           = 1;
    Direct.colors_max           = 1;
    Direct.colors.resize(1);
    Direct.brightness_min       = ZOTAC_GPU_BRIGHTNESS_MIN;
    Direct.brightness           = ZOTAC_GPU_BRIGHTNESS_DEFAULT;
    Direct.brightness_max       = ZOTAC_GPU_BRIGHTNESS_MAX;
    modes.push_back(Direct);

    mode Breathing;
    Breathing.name = "Breathing";
    Breathing.value = ZOTAC_GPU_MODE_BREATHING;
    Breathing.flags = MODE_FLAG_HAS_MODE_SPECIFIC_COLOR;
    Breathing.colors_min        = 1;
    Breathing.colors_max        = 1;
    Breathing.colors.resize(1);
    Breathing.color_mode = MODE_COLORS_MODE_SPECIFIC;
    modes.push_back(Breathing);

    mode Flashing;
    Flashing.name = "Flashing";
    Flashing.value = ZOTAC_GPU_MODE_STROBE;
    Flashing.flags = MODE_FLAG_HAS_MODE_SPECIFIC_COLOR;
    Flashing.colors_min        = 1;
    Flashing.colors_max        = 1;
    Flashing.colors.resize(1);
    Flashing.color_mode = MODE_COLORS_MODE_SPECIFIC;
    modes.push_back(Flashing);

    mode ColorCycle;
    ColorCycle.name = "Spectrum Cycle";
    ColorCycle.value = ZOTAC_GPU_MODE_COLOR_CYCLE;
    ColorCycle.color_mode = MODE_COLORS_NONE;
    modes.push_back(ColorCycle);

    mode Off;
    Off.name = "Off";
    Off.value = ZOTAC_GPU_BRIGHTNESS_MIN;
    Off.color_mode = MODE_COLORS_NONE;
    modes.push_back(Off);

    SetupZones();
}

RGBController_ZotacGTX1070::~RGBController_ZotacGTX1070()
{
    delete controller;
}

void RGBController_ZotacGTX1070::SetupZones()
{
    /*---------------------------------------------------------*/
    /* Set up zones                                            */
    /*---------------------------------------------------------*/
    zone new_zone;
    led  new_led;

    zone zotac_gtx1070_logo;
    zotac_gtx1070_logo.name = "ZOTAC LOGO";
    zotac_gtx1070_logo.type = ZONE_TYPE_SINGLE;
    zotac_gtx1070_logo.leds_min = 1;
    zotac_gtx1070_logo.leds_max = 1;
    zotac_gtx1070_logo.leds_count = 1;
    zotac_gtx1070_logo.matrix_map = NULL;
    zones.push_back(zotac_gtx1070_logo);

    zone zotac_gtx1070_push_the_limit;
    zotac_gtx1070_push_the_limit.name = "PUSH THE LIMIT LOGO";
    zotac_gtx1070_push_the_limit.type = ZONE_TYPE_SINGLE;
    zotac_gtx1070_push_the_limit.leds_min = 1;
    zotac_gtx1070_push_the_limit.leds_max = 1;
    zotac_gtx1070_push_the_limit.leds_count = 1;
    zotac_gtx1070_push_the_limit.matrix_map = NULL;
    zones.push_back(zotac_gtx1070_push_the_limit); 

    leds.push_back(new_led);
    leds.push_back(new_led);

    SetupColors();
    SetupInitialValues(); 
}


void RGBController_ZotacGTX1070::SetupInitialValues()
{
    /*---------------------------------------------------------*\
    | Retrieve current values by reading the device             |
    \*---------------------------------------------------------*/

    /*controller->GetMode();*/
    SignalUpdate();
}

void RGBController_ZotacGTX1070::ResizeZone(int /*zone*/, int /*new_size*/)
{
    /*---------------------------------------------------------*\
    | This device does not support resizing zones               |
    \*---------------------------------------------------------*/
}

void RGBController_ZotacGTX1070::DeviceUpdateLEDs()
{
    DeviceUpdateMode();
}

void RGBController_ZotacGTX1070::UpdateZoneLEDs(int /*zone*/)
{
    DeviceUpdateMode();
}

void RGBController_ZotacGTX1070::UpdateSingleLED(int /*led*/)
{
    DeviceUpdateMode();
}

void RGBController_ZotacGTX1070::DeviceUpdateMode()
{
    controller->SetMode(colors[0], modes[active_mode].value, modes[active_mode].brightness);
}
