/*-----------------------------------------*\
|  ZotacGTX1070ControllerDetect.cpp         |
|                                           |
|  Generic RGB Interface For OpenRGB        |
|  ZOTAC GTX 1070                           |
|                                           |
|  Martin Solohaga (Martinchoox) 12/18/2023 |
\*-----------------------------------------*/

#include "Detector.h"
#include "ZotacGTX1070Controller.h"
#include "RGBController.h"
#include "RGBController_ZotacGTX1070.h"
#include "i2c_smbus.h"
#include "pci_ids.h"

void DetectZotacGTX1070Controllers(i2c_smbus_interface* bus, u8 i2c_addr, const std::string& name)
{
    if(bus->port_id == 1)
    {
        ZotacGTX1070Controller*     controller     = new ZotacGTX1070Controller(bus, i2c_addr);
        RGBController_ZotacGTX1070* rgb_controller = new RGBController_ZotacGTX1070(controller);
        rgb_controller->name                         = name;

        ResourceManager::get()->RegisterRGBController(rgb_controller);
    }
}

REGISTER_I2C_PCI_DETECTOR("ZOTAC GTX 1070 AMP Extreme Edition"                        , DetectZotacGTX1070Controllers, NVIDIA_VEN,     NVIDIA_GTX1070_DEV,     ZOTAC_SUB_VEN,   ZOTAC_GTX1070_AMP_EXTREME_SUB_DEV,             0x48);