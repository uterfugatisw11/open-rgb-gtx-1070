/*-----------------------------------------*\
|  RGBController_ZotacGTX1070.h             |
|                                           |
|  Generic RGB Interface For OpenRGB        |
|  ZOTAC GTX 1070                           |
|                                           |
|  Martin Solohaga (Martinchoox) 12/18/2023 |
\*-----------------------------------------*/

#pragma once

#include "RGBController.h"
#include "ZotacGTX1070Controller.h"


class RGBController_ZotacGTX1070 : public RGBController
{
public:
    RGBController_ZotacGTX1070(ZotacGTX1070Controller* controller_ptr);
    ~RGBController_ZotacGTX1070();

    void SetupInitialValues();
    void SetupZones();

    void ResizeZone(int zone, int new_size);

    void DeviceUpdateLEDs();
    void UpdateZoneLEDs(int zone);
    void UpdateSingleLED(int led);

    void DeviceUpdateMode();

private:
    ZotacGTX1070Controller* controller;
};
