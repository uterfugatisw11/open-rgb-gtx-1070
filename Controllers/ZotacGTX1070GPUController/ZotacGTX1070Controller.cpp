/*-----------------------------------------*\
|  ZotacGTX1070Controller.cpp               |
|                                           |
|  Generic RGB Interface For OpenRGB        |
|  ZOTAC GTX 1070                           |
|                                           |
|  Martin Solohaga (Martinchoox) 12/18/2023 |
\*-----------------------------------------*/

#include "ZotacGTX1070Controller.h"
#include "LogManager.h"
#include <string.h>
#include <cmath>

static unsigned char argb_colour_index_data[2][2][2] =
    {
        // B0    B1
        {
            {0x00, 0x04}, // G0 R0
            {0x02, 0x03},
        }, // G1 R0
        {
            {0x00, 0x05}, // G0 R1
            {0x01, 0x06},
        } // G1 R1
};

ZotacGTX1070Controller::ZotacGTX1070Controller(i2c_smbus_interface *bus, u8 dev)
{
    this->bus = bus;
    this->dev = dev;
}

ZotacGTX1070Controller::~ZotacGTX1070Controller()
{
}

std::string ZotacGTX1070Controller::GetDeviceLocation()
{
    std::string return_string(bus->device_name);
    char addr[5];

    snprintf(addr, 5, "0x%02X", dev);
    return_string.append(", address ");
    return_string.append(addr);
    return ("I2C: " + return_string);
}


unsigned int ZotacGTX1070Controller::GetLargestColour(unsigned int red, unsigned int green, unsigned int blue)
{
    unsigned int largest;

    if (red > green)
    {
        (red > blue) ? largest = red : largest = blue;
    }
    else
    {
        (green > blue) ? largest = green : largest = blue;
    }

    return (largest == 0) ? 1 : largest;
}

unsigned char ZotacGTX1070Controller::GetColourIndex(unsigned char red, unsigned char green, unsigned char blue)
{
    /*-----------------------------------------------------*\
    | This device uses a limited colour pallette referenced |
    | by an index                                           |
    | 0x00 red                                              |
    | 0x01 yellow                                           |
    | 0x02 green                                            |
    | 0x03 cyan                                             |
    | 0x04 blue                                             |
    | 0x05 magen                                            |
    | 0x06 white                                            |
    \*-----------------------------------------------------*/

    unsigned int divisor = GetLargestColour(red, green, blue);
    unsigned int r = round(red / divisor);
    unsigned int g = round(green / divisor);
    unsigned int b = round(blue / divisor);
    unsigned char idx = argb_colour_index_data[r][g][b];
    return idx;
}

void ZotacGTX1070Controller::SetMode(RGBColor color, int mode, int brightness)
{
    u8 data_pkt[] =
    {
        (u8)mode,
        (u8)GetColourIndex(RGBGetRValue(color),RGBGetGValue(color),RGBGetBValue(color)),
        (u8)GetColourIndex(RGBGetRValue(color),RGBGetGValue(color),RGBGetBValue(color)),
        (u8)brightness
    };
    
    LOG_DEBUG("[%s] I2C_TEST:",dev, 0x04, data_pkt);
    /*bus->i2c_write_block(dev, 0x04, data_pkt);*/

    /*---------------------------------------------------------*\
    | Read back color and mode. Not doing this seems to hang    |
    | the RGB controller device when switching mode...          |
    \*---------------------------------------------------------*/
    /*GetMode(color, mode, brightness);*/
}