/*-----------------------------------------*\
|  ZotacGTX1070Controller.h                 |
|                                           |
|  Generic RGB Interface For OpenRGB        |
|  ZOTAC GTX 1070                           |
|                                           |
|  Martin Solohaga (Martinchoox) 12/18/2023 |
\*-----------------------------------------*/

#include <string>
#include "i2c_smbus.h"
#include "RGBController.h"

#pragma once

#define ZOTAC_GPU_LEDS_COUNT   6

enum
{
    ZOTAC_GPU_MODE_STATIC = 0xF0,
    ZOTAC_GPU_MODE_BREATHING = 0xF1,
    ZOTAC_GPU_MODE_STROBE = 0xF2,
    ZOTAC_GPU_MODE_COLOR_CYCLE = 0xF3,
};

enum
{
    ZOTAC_GPU_BRIGHTNESS_MIN = 0x00,
    ZOTAC_GPU_BRIGHTNESS_DEFAULT = 0xE0,
    ZOTAC_GPU_BRIGHTNESS_MAX = 0xE0,
};

class ZotacGTX1070Controller
{
public:
    ZotacGTX1070Controller(i2c_smbus_interface* bus, u8 dev);
    ~ZotacGTX1070Controller();

    std::string GetDeviceLocation();

    void GetMode();
    void SetMode(RGBColor color, int mode, int brightness);

private:
    unsigned int GetLargestColour(unsigned int red, unsigned int green, unsigned int blue);
    unsigned char GetColourIndex(unsigned char red, unsigned char green, unsigned char blue);

    i2c_smbus_interface* bus;
    u8 dev;
};
